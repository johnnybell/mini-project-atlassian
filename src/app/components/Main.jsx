import React, { Component } from 'react';
import { connect } from 'react-redux';
import data from './data/sessions.json';
import styled from 'styled-components';

// Main Content Area
const MainPanel = styled.div`
    width: 70%;
    float: right;
    h3 {
        color: #005183;
        font-size: 25px;
        padding: 0;
        margin: 5px 0 10px 0;
    }
    span {
        color: #005183;
        font-size: 12px;
        font-weight: bold;
    }
    p {
        font-size: 14px;
        line-height: 20px;
    }
`;

class Main extends Component {
    loadMainItems() {
        // Pull in the setTrack from Redux
        const { setTrack, currentIndex } = this.props;
        // Create the empty array to push too
        let mainItems = [];
        // Map through the object to get the data
        Object.keys(data.Items).map(item => {
            // Only render if the user is on the correct page
            if (data.Items[item].Track.Title === setTrack) {
                // Only show if there are speakers
                if (data.Items[item].Speakers) {
                    // Push main items to the array, the title, speakers, company and finally description.
                    mainItems.push(
                        <div key={item.AttendeeID}>
                            <h3>{data.Items[item].Title}</h3>
                            <span>
                                {data.Items[item].Speakers[0].FirstName} {data.Items[item].Speakers[0].LastName}
                                {', '}
                                {data.Items[item].Speakers[0].Company}
                            </span>
                            <p>{data.Items[item].Description}</p>
                        </div>
                    );
                }
            }
        });
        // Return main items with currentIndex so you just show one not all of them
        return mainItems[currentIndex];
    }

    render() {
        return <MainPanel>{this.loadMainItems()}</MainPanel>;
    }
}

// Pull in the setTrack, currentIndex prop from Redux
const mapStateToProps = state => ({
    setTrack: state.setTrack,
    currentIndex: state.currentIndex
});

export default connect(mapStateToProps)(Main);
