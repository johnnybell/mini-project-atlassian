import { createStore } from 'redux';
import reducer from './reducers';
// Include Redux dev tools
import { composeWithDevTools } from 'redux-devtools-extension';

const store = createStore(reducer, composeWithDevTools());

export default store;
