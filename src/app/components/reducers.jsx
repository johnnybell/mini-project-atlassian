import { SAVE_TRACK, SAVE_CURRENT_INDEX } from './actions';

const DEFAULT_STATE = {
    // Set this to the first link
    setTrack: 'Build',
    // Set this to 0 so its the first index
    currentIndex: 0
};

const saveTrack = (state, action) => Object.assign({}, state, { setTrack: action.payload });
const saveCurrentIndex = (state, action) => Object.assign({}, state, { currentIndex: action.payload });

const rootReducer = (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case SAVE_TRACK:
            return saveTrack(state, action);
        case SAVE_CURRENT_INDEX:
            return saveCurrentIndex(state, action);
        default:
            return state;
    }
};

export default rootReducer;
