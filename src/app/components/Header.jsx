import React, { Component } from 'react';
import styled from 'styled-components';
import logo from './svgs';
import Summit from '../assets/img/summit.png';

// Top header panel
const HeaderPanel = styled.div`
    height: 50px;
    width: 100%;
    background: #005183;
    font-family: sans-serif;
    svg {
        height: 20px;
        padding: 12px 0 0 10px;
        fill: white;
    }
`;

// Middle header image / title
const SummitTitle = styled.div`
    text-align: center;
    margin: 50px 0;
    p {
        font-size: 18px;
    }
`;

class Header extends Component {
    render() {
        return (
            <div>
                <HeaderPanel>
                    <a
                        href="https://www.atlassian.com/company/events/summit-us"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        {logo}
                    </a>
                </HeaderPanel>
                <SummitTitle>
                    <img src={Summit} alt="Atlassian Summit" />
                    <p>Video Archive</p>
                </SummitTitle>
            </div>
        );
    }
}

export default Header;
