import React, { Component } from 'react';
import { connect } from 'react-redux';
import data from './data/sessions.json';
import styled from 'styled-components';
import _ from 'lodash';
import { saveCurrentIndex, saveTrack } from './actionCreators';

// The Navigation
const MenuItem = styled.ul`
    margin: 0 0 25px 0;
    padding: 0;
    border-bottom: 1px solid #cccccc;
    li {
        display: inline-block;
        padding: 10px 20px;
        background: #f5f5f5;
        font-size: 13px;
        border-top: 1px solid #cccccc;
        border-left: 1px solid #cccccc;
        cursor: pointer;
        margin-bottom: -1px;
        &:last-child {
            border-right: 1px solid #cccccc;
            border-top-right-radius: 5px;
        }
        &:first-child {
            border-top-left-radius: 5px;
        }
        &.active {
            background: white;
            border-bottom: 1px solid white;
        }
    }
`;

class Menu extends Component {
    loadMenu() {
        // Pull in the setTrack, and saveTrack from Redux
        const { setTrack, saveTrack, saveCurrentIndex } = this.props;
        let menuItems = [];
        // Used to create a unique key as the item not was unique
        const generateKey = pre => {
            return `${pre}_${new Date().getTime()}`;
        };
        // Find the track.title for the menu items from the data
        Object.keys(data.Items).map(item => menuItems.push(data.Items[item].Track.Title));
        // As there are many of these remove the duplicates with lodash
        const removeDupes = _.uniq(menuItems);
        // Map over these and break them out to their own LI
        const finalMenuItems = removeDupes.map(item => (
            <li
                // If the current menu is active, add a class
                className={setTrack === item ? 'active' : ''}
                onClick={() => {
                    // On click push the track (menu item) to Redux to get the nav working
                    saveTrack(item);
                    saveCurrentIndex(0);
                }}
                // Generate the key with the generateKey function created above
                key={generateKey(item)}
            >
                {item}
            </li>
        ));
        // Return the menu items without the duplicates
        return <MenuItem>{finalMenuItems}</MenuItem>;
    }
    render() {
        return <div>{this.loadMenu()}</div>;
    }
}

// Pull in the setTrack prop from Redux
const mapStateToProps = state => ({
    setTrack: state.setTrack
});
// Push in the saveTrack & saveCurrentIndex prop to Redux
const mapDispatchToProps = dispatch => ({
    saveTrack: data => dispatch(saveTrack(data)),
    saveCurrentIndex: data => dispatch(saveCurrentIndex(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
