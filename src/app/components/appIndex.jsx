import React from 'react';
import styled, { injectGlobal } from 'styled-components';
import Header from './Header';
import Menu from './Menu';
import Sidebar from './Sidebar';
import Main from './Main';

// Global Styling
injectGlobal`
  body {
    margin: 0;
    font-family: sans-serif;
  }
`;

// Main Wrapper
const Wrapper = styled.div`
    width: 960px;
    margin: 0 auto;
`;

class AppIndex extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <Wrapper>
                    <Menu />
                    <Sidebar />
                    <Main />
                </Wrapper>
            </div>
        );
    }
}

export default AppIndex;
