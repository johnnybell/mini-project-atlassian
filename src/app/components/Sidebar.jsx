import React, { Component } from 'react';
import data from './data/sessions.json';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { saveCurrentIndex } from './actionCreators';

// Sidebar Wrapper
const SidebarPanel = styled.div`
    width: 25%;
    float: left;
    margin-right: 5%;
    h2 {
        color: #005183;
        font-size: 18px;
        padding: 0;
    }
`;

// Sidebar Items
const SidebarItems = styled.div`
    .sidebar-item {
        cursor: pointer;
        padding-bottom: 10px;
        h3 {
            color: #005183;
            font-size: 15px;
            border-top: 1px solid #cccccc;
            padding: 15px 10px 0 10px;
            margin: 0 0 5px 0;
        }
        .speakers {
            color: #707070;
            font-size: 12px;
            padding: 0 10px 0 10px;
        }
    }
    .active {
        background: #f5f5f5;
    }
`;

class Sidebar extends Component {
    onItemClick(index) {
        const { saveCurrentIndex } = this.props;
        saveCurrentIndex(index);
    }
    loadSidebarItems() {
        // Create empty array for the Sidebar Items
        let mainItems = [];
        // Pull in the setTrack from Redux
        const { setTrack, currentIndex } = this.props;
        // Map though the items in the data to collect the sidebar items
        Object.keys(data.Items).map(item => {
            // Only render if the user is on the correct page
            if (data.Items[item].Track.Title === setTrack) {
                // Only show if there are speakers
                if (data.Items[item].Speakers) {
                    // Push sidebar items to the array, the title, speakers and company.
                    mainItems.push(
                        // Using Div instead of styled-components because I get errors with push and styled-components :(
                        <div
                            key={item}
                            className={currentIndex === mainItems.length ? 'sidebar-item active' : 'sidebar-item'}
                            onClick={this.onItemClick.bind(this, mainItems.length)}
                        >
                            <h3>{data.Items[item].Title}</h3>
                            <div className="speakers">
                                {data.Items[item].Speakers[0].FirstName} {data.Items[item].Speakers[0].LastName}
                                {', '}
                                {data.Items[item].Speakers[0].Company}
                            </div>
                        </div>
                    );
                }
            }
        });
        // Return the sidebar items
        return mainItems;
    }

    render() {
        return (
            <SidebarPanel>
                <h2>Software Teams</h2>
                <SidebarItems>{this.loadSidebarItems()}</SidebarItems>
            </SidebarPanel>
        );
    }
}

// Push in the saveCurrentIndex prop to Redux
const mapDispatchToProps = dispatch => ({
    saveCurrentIndex: data => dispatch(saveCurrentIndex(data))
});

// Pull in the setTrack & currentIndex prop from Redux
const mapStateToProps = state => ({
    setTrack: state.setTrack,
    currentIndex: state.currentIndex
});

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
