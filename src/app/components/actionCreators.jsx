import { SAVE_TRACK, SAVE_CURRENT_INDEX } from './actions';

export function saveTrack(track) {
    return { type: SAVE_TRACK, payload: track };
}
export function saveCurrentIndex(index) {
    return { type: SAVE_CURRENT_INDEX, payload: index };
}
