import React from 'react';
import ReactDOM from 'react-dom';
import 'whatwg-fetch';
import 'babel-polyfill';
import { Provider } from 'react-redux';
import store from './app/components/store';

//components
import AppIndex from './app/components/appIndex';

ReactDOM.render(
    <Provider store={store}>
        <AppIndex />
    </Provider>,
    document.getElementById('app')
);
